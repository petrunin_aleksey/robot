package ru.pae.robot;

public enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT
}
